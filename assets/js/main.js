(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

})(jQuery);
// <--------------------------functioning for toast--------------------------->
function myFunction() {
	var x = document.getElementById("snackbar");
	x.className = "show";// Adding "show" class to DIV
	setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }
  function myFunction1() {
	var x = document.getElementById("snackbar1");
	x.className = "show";// Adding "show" class to DIV
	setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }
  function lib_toast() {
	var x = document.getElementById("up_snackbar_lib");
	x.className = "show";// Adding "show" class to DIV
	setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }

// <-------------------------functioning for customer page-------------------->
function content() {
	var p = document.getElementById("profile1");
	p.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "none";
	var c = document.getElementById("content1");
	c.style.display = "block";
  }
  function profile() {
	var c = document.getElementById("content1");
	c.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "none";
	var p = document.getElementById("profile1");
	p.style.display = "block";
  }
  function lend() {
	var p = document.getElementById("profile1");
	p.style.display = "none";
	var c = document.getElementById("content1");
	c.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "block";
  }
  function returnbook() {
	var p = document.getElementById("profile1");
	p.style.display = "none";
	var c = document.getElementById("content1");
	c.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "block";
  }
  function pending() {
	var p = document.getElementById("profile1");
	p.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "none";
	var c = document.getElementById("content1");
	c.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "block";
  }
  function review() {
	var p = document.getElementById("profile1");
	p.style.display = "none";
	var l = document.getElementById("lend1");
	l.style.display = "none";
	var r = document.getElementById("returnbook1");
	r.style.display = "none";
	var c = document.getElementById("content1");
	c.style.display = "none";
	var pend = document.getElementById("pending1");
	pend.style.display = "none";
	var rev = document.getElementById("review1");
	rev.style.display = "block";
  }
  // <-------------------------functioning for librarian-------------->
  function lib_profile() {
	var p = document.getElementById("profile2");
	p.style.display = "block";
	var a = document.getElementById("add1");
	a.style.display = "none";
	var r = document.getElementById("remove1");
	r.style.display = "none";
	var e = document.getElementById("edit1");
	e.style.display = "none";
	var c = document.getElementById("content2");
	c.style.display = "none";
  }
  function lib_addbook() {
	var p = document.getElementById("profile2");
	p.style.display = "none"
	var a = document.getElementById("add1");
	a.style.display = "block";
	var r = document.getElementById("remove1");
	r.style.display = "none";
	var e = document.getElementById("edit1");
	e.style.display = "none";
	var c = document.getElementById("content2");
	c.style.display = "none";
  }
  function lib_editbook() {
	var p = document.getElementById("profile2");
	p.style.display = "none"
	var a = document.getElementById("add1");
	a.style.display = "none";
	var r = document.getElementById("remove1");
	r.style.display = "none";
	var e = document.getElementById("edit1");
	e.style.display = "block";
	var c = document.getElementById("content2");
	c.style.display = "none";
  }
  function lib_removebook() {
	var p = document.getElementById("profile2");
	p.style.display = "none"
	var a = document.getElementById("add1");
	a.style.display = "none";
	var r = document.getElementById("remove1");
	r.style.display = "block";
	var e = document.getElementById("edit1");
	e.style.display = "none";
	var c = document.getElementById("content2");
	c.style.display = "none";
  }
  function content2() {
	var p = document.getElementById("profile2");
	p.style.display = "none"
	var a = document.getElementById("add1");
	a.style.display = "none";
	var r = document.getElementById("remove1");
	r.style.display = "none";
	var e = document.getElementById("edit1");
	e.style.display = "none";
	var c = document.getElementById("content2");
	c.style.display = "block";
  }
  // <-------------------------functioning for co-operatinglibrarian----------------------> 
  function content3() {
	var c = document.getElementById("content3");
	c.style.display = "block";
	var cp = document.getElementById("profile3");
	cp.style.display = "none";
	var r = document.getElementById("request1");
	r.style.display = "none";
  }
  function co_profile() {
	var c = document.getElementById("content3");
	c.style.display = "none";
	var cp = document.getElementById("profile3");
	cp.style.display = "block";
	var r = document.getElementById("request1");
	r.style.display = "none";
  }
  function co_requestbook() {
	var c = document.getElementById("content3");
	c.style.display = "none";
	var cp = document.getElementById("profile3");
	cp.style.display = "none";
	var r = document.getElementById("request1");
	r.style.display = "block";
  }
  // <------------------------------------reset------------------------------------------->
  function reset() {
	FormData.reset;
  }
  // <------------------------------------update for customer------------------------------------------>
  function update1(){
	var up = document.getElementById("up_customer");
	up.style.display = "block";
	
  }
  function update_customer(){
	
	var email = document.getElementById("up_email").value;
	alert(email.length); 
	var error_email = document.getElementById("up_error_email");
	var username = document.getElementById('username1').value;
	var error_username = document.getElementById('error_uname2');
	var password = document.getElementById("password3").value;
	var error_password = document.getElementById("error_pass3");
	var password1 = document.getElementById("password4").value;
	var error_pass1 = document.getElementById("error_pass4");
	var fullname = document.getElementById("firstname1").value;
	var error_fname = document.getElementById("error_fname1");
	var age = document.getElementById("age1").value;
	var error_age = document.getElementById("error_age1");
	var address = document.getElementById("textarea2").value;
	var error_address = document.getElementById("error_address2");
	var state = document.getElementById("state1").value;
	var error_state = document.getElementById("error_state1");
	var city = document.getElementById("city1").value;
	var error_city = document.getElementById("error_city1");
  
	alert(email.length);
  if(email.length === 0){
	  error_email.innerText = "please enter your Email Id"
  }else{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (email.match(mailformat)){
	  error_email.innerText = ""
	}
	else{
	  error_email.innerText = "Please enter the valid Emailid"
	}
  }
  
	if (username.length === 0) {
	  error_username.innerText = "Please enter the Username"
	} else {
	  error_username.innerText = ""
	}
  
	if (password.length === 0) {
	  error_password.innerText = "Please enter the Password"
	} else { 
	  if (password.match(/[a-z]/g) && password.match(
		/[A-Z]/g) && password.match(
		  /[0-9]/g) && password.match(
			/[^a-zA-Z\d]/g) && password.length >= 6)
		error_password.innerText = ""
	  else
		error_password.innerText = "Password must contain atleast 1 number,1 Uppercase and Lower case letter ,1 special character and minimun 6 Characters."
	}
  
	if (password1.length === 0) {
	  error_pass1.innerText = "Please retype the password"
	} else {
	  error_pass1.innerText = ""
	}
	if (password1.length === 0) {
	  error_pass1.innerText = "Please retype the password"
	} else {
	  if(password != password1)
		  error_pass1.innerText = "Password doesn't match";
	  else
		  error_pass1.innerText = ""
	}
	
  
	if (fullname.length === 0) {
	  error_fname.innerText = "Please enter your fullname"
	} else {
	  error_fname.innerText = ""
	}
	if (age.length === 0) {
	  error_age.innerText = "*Please enter your age here"
	} else {
	  error_age.innerText = ""
	}
	if(address.length === 0){
	  error_address.innerText = "Please enter your address"
	}else{
	  error_address.innerText = ""
	}
  
	if (state.length === 0) {
	  error_state.innerText = "Please select the State"
	} else {
	  error_state.innerText = ""
	}
  
	if (city.length === 0) {
	  error_city.innerText = "Please select the City"
	} else {
	  error_city.innerText = ""
	}
	if (email && username && password && password1 && fullname && age && address && state && city) {
	  alert("done");
	  var Obj3 = { 'Email': email, 'Username': username, 'Password': password, 'Name': fullname, 'Age': age, 'Address': address, 'State': state, 'City': city}
	  request_data(Obj3, "PUT", username_value);
	  var up = document.getElementById("up_customer");
	  up.style.display = "none";
	}
  }
//   <--------------------------------update for librarian------------------------------------------->
function update2(){
	var up = document.getElementById("up_librarian");
	up.style.display = "block";
}
function update_librarian(){
	alert("lib");
	var email = document.getElementById("up_email2").value;
	alert(email.length); 
	var error_email = document.getElementById("up_error_email2");
	var username = document.getElementById('up_uname2').value;
	var error_username = document.getElementById('up_error_uname2');
	var password = document.getElementById("up_password3").value;
	var error_password = document.getElementById("up_error_pass3");
	var password1 = document.getElementById("up_password4").value;
	var error_pass1 = document.getElementById("up_error_pass4");
	var fullname = document.getElementById("up_fname2").value;
	var error_fname = document.getElementById("up_error_fname2");
	var age = document.getElementById("up_age2").value;
	var error_age = document.getElementById("up_error_age2");
	var address = document.getElementById("up_address2").value;
	var error_address = document.getElementById("up_error_address2");
	var state = document.getElementById("up_state2").value;
	var error_state = document.getElementById("up_error_state2");
	var city = document.getElementById("up_city2").value;
	var error_city = document.getElementById("up_error_city2");
  
	alert(email.length);
  if(email.length === 0){
	  error_email.innerText = "please enter your Email Id"
  }else{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (email.match(mailformat)){
	  error_email.innerText = ""
	}
	else{
	  error_email.innerText = "Please enter the valid Emailid"
	}
  }
  
	if (username.length === 0) {
	  error_username.innerText = "Please enter the Username"
	} else {
	  error_username.innerText = ""
	}
  
	if (password.length === 0) {
	  error_password.innerText = "Please enter the Password"
	} else { 
	  if (password.match(/[a-z]/g) && password.match(
		/[A-Z]/g) && password.match(
		  /[0-9]/g) && password.match(
			/[^a-zA-Z\d]/g) && password.length >= 6)
		error_password.innerText = ""
	  else
		error_password.innerText = "Password must contain atleast 1 number,1 Uppercase and Lower case letter ,1 special character and minimun 6 Characters."
	}
  
	if (password1.length === 0) {
	  error_pass1.innerText = "Please retype the password"
	} else {
	  error_pass1.innerText = ""
	}
	if (password1.length === 0) {
	  error_pass1.innerText = "Please retype the password"
	} else {
	  if(password != password1)
		  error_pass1.innerText = "Password doesn't match";
	  else
		  error_pass1.innerText = ""
	}
	
  
	if (fullname.length === 0) {
	  error_fname.innerText = "Please enter your fullname"
	} else {
	  error_fname.innerText = ""
	}
	if (age.length === 0) {
	  error_age.innerText = "*Please enter your age here"
	} else {
	  error_age.innerText = ""
	}
	if(address.length === 0){
	  error_address.innerText = "Please enter your address"
	}else{
	  error_address.innerText = ""
	}
  
	if (state.length === 0) {
	  error_state.innerText = "Please select the State"
	} else {
	  error_state.innerText = ""
	}
  
	if (city.length === 0) {
	  error_city.innerText = "Please select the City"
	} else {
	  error_city.innerText = ""
	}
	if (email && username && password && password1 && fullname && age && address && state && city) {
	  alert("requesting update");
	  var Obj3 = { 'Email': email, 'Username': username, 'Password': password, 'Name': fullname, 'Age': age, 'Address': address, 'State': state, 'City': city}
	  request_data(Obj3, "PUT", username_value);
	  var up = document.getElementById("up_librarian");
	  up.style.display = "none";
	}
}
//   <--------------------------------update for co-operating librarian------------------------------->
function update3(){

}
function update_colibrarian(){
	
}