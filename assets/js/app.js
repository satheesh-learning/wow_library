function start() {
  document.getElementById('idlogin').style.display = 'block';
  document.getElementById('idregister').style.display = 'none';
}
function registerpage() {
  document.getElementById('idregister').style.display = 'block';
  document.getElementById('idlogin').style.display = 'none';
}
function loginpage() {
  document.getElementById('idlogin').style.display = 'block';
  document.getElementById('idregister').style.display = 'none';
}

var resObj1, resObj2;
function register() {
  var role = document.getElementById('role').value;
  var error_role = document.getElementById('error_role');
  var email = document.getElementById('email').value;
  var error_email = document.getElementById('error_email');
  var username = document.getElementById('username').value;
  var error_user = document.getElementById('error_uname1');
  var password = document.getElementById('password1').value;
  var error_password = document.getElementById('error_pass1');
  var password2 = document.getElementById('password2').value;
  var error_pass2 = document.getElementById('error_pass2');
  var fullname = document.getElementById('firstname').value;
  var error_fname = document.getElementById('error_fname');
  if (document.getElementById('radio1').checked) {
    var gender = document.getElementById('radio1').value;
  }
  else {
    var gender = document.getElementById('radio2').value;
  }
  var age = document.getElementById('age').value
  var error_age = document.getElementById('error_age');
  var address = document.getElementById('textarea1').value;
  var error_address = document.getElementById('error_address');
  var state = document.getElementById('state').value;
  var error_state = document.getElementById('error_state');
  var city = document.getElementById('city').value;
  var error_city = document.getElementById('error_city');
  var idtype = document.getElementById('idtype').value;
  var error_idtype = document.getElementById('error_idtype');

  if (role.length === 0) {
    error_role.innerText = "Please select the Role"
  } else {
    error_role.innerText = ""
  }

  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (email.match(mailformat))
    error_email.innerText = ""
  else
    error_email.innerText = "Please enter the valid Emailid"


  if (username.length === 0) {
    error_user.innerText = "Please enter the Username"
  } else {
    error_user.innerText = ""
  }

  if (password.length === 0) {
    error_password.innerText = "Please enter the Password"
  } else {
    if (password.match(/[a-z]/g) && password.match(
      /[A-Z]/g) && password.match(
        /[0-9]/g) && password.match(
          /[^a-zA-Z\d]/g) && password.length >= 6)
      error_password.innerText = ""
    else
      error_password.innerText = "Password must contain atleast 1 number,1 Uppercase and Lower case letter ,1 special character and minimun 6 Characters."
  }

  if (password2.length === 0) {
    error_pass2.innerText = "Please retype the password"
  } else {
    if(password != password2)
        error_pass2.innerText = "Password doesn't match";
    else
        error_pass2.innerText = ""
  }

  if (fullname.length === 0) {
    error_fname.innerText = "Please enter your fullname"
  } else {
    error_fname.innerText = ""
  }
  if (age.length === 0) {
    error_age.innerText = "*Please enter your age here"
  } else {
    error_age.innerText = ""
  }
  if(address.length === 0){
    error_address.innerText = "Please enter your address"
  }else{
    error_address.innerText = ""
  }

  if (state.length === 0) {
    error_state.innerText = "Please select the State"
  } else {
    error_state.innerText = ""
  }

  if (city.length === 0) {
    error_city.innerText = "Please select the City"
  } else {
    error_city.innerText = ""
  }

  if (idtype.length === 0) {
    error_idtype.innerText = "Please select the IdType"
  } else {
    error_idtype.innerText = ""
  }

  var Obj1 = {
    "Role": role, "Email": email, "Username": username,
    "Password": password, "Name": fullname,
    "Gender": gender, "Age": age, "Address": address,
    "State": state, "City": city, "Idtype": idtype
  }
  request_data(Obj1, "POST", "register");

}

function login() {
  const user = document.getElementById("user").value;
  var error_user = document.getElementById("error_uname");
  var password = document.getElementById("password").value;
  var error_password = document.getElementById("error_pass");
  var Focus = false;
  if (user.length === 0) {
    error_user.innerText = "Please enter the Username"
    Focus = true;
    

  } else {
    error_user.innerText = ""
  }

  if (password.length === 0) {
    error_password.innerText = "Please enter the Password"
    if (!Focus) {
      password.focus();
    }
  } else {
    if (password.match(/[a-z]/g) && password.match(
      /[A-Z]/g) && password.match(
        /[0-9]/g) && password.match(
          /[^a-zA-Z\d]/g) && password.length >= 6)
      error_password.innerText = ""
    else
      error_password.innerText = "Password must contain atleast 1 number,1 Uppercase and Lower case letter ,1 special character and minimun 6 Characters."
  }
  
  if (user && password) {
    var Obj2 = { 'Username': user, 'Password': password }
    request_data(Obj2, "POST", "login");
  }
}

function update_values(x) {
  var myObj2 = { 'password': p_wd }

  request_data(myObj2, type = "PUT", url = x);
  alert("submitted");
}

var username_value = localStorage.getItem("Username");
function request_data(para = "", type = "GET", url = "") {
alert(type)
  var table = document.getElementById("profile_table");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4) {
      // if (this.status === 401) {
      //   alert("UnAuthorized");
      //   }

      if (this.status === 200) {
        alert("200");
        var myObj = JSON.parse(this.responseText);
        myObj.forEach((record) => {
          if (type === "GET" || type === "PUT") {
            display_Data(table, record);
            if(type === "PUT")
            {
              myFunction1();
            }
          }
        });
        localStorage.setItem("Username",para.Username);
        if (url == "login") {
          resObj1 = JSON.parse(this.responseText);
          go_to(type, resObj1[0].Role);
        }
        if (url == "register") {
          alert("registration successfull");
          resObj2 = JSON.parse(this.responseText);
          go_to(type, resObj2[0].Role);
        }

      }
      else {
        myFunction();
      }
    }
  }
  xhttp.open(type, `http://localhost:3014/${url}`, true);//true is for async , false for sync
  xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  if (type === "POST" ||type === "PUT") {
    xhttp.send(JSON.stringify(para));
  }
  else {
    xhttp.send();
  }
}
function go_to(type, Passed_role) {
  var role_type = Passed_role;
  if (role_type == "Customer") {
    window.location.href = "./customer.html";
  }
  if (role_type == "Librarian") {
    window.location.href = "./librarian.html";
  }
  if (role_type == "Co-operatinglibrarian") {
    window.location.href = "./co-librarian.html";
  }

}
// <-----------------------------inserting values to client page------------------------------->
function display_Data(table, record) {

  // idnumber=idnumber+1;   
  var row = table.insertRow(1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
  var cell8 = row.insertCell(7);


  cell1.innerHTML = record.Email;
  cell2.innerHTML = record.Username;
  cell3.innerHTML = record.Password;
  cell4.innerHTML = record.Name;
  cell5.innerHTML = record.Age;
  cell6.innerHTML = record.Address;
  cell7.innerHTML = record.State;
  cell8.innerHTML = record.City;
}

