const express = require('express');
const bodyparse = require('body-parser');
const app = express();
app.use(bodyparse.urlencoded({ extended: false }));

app.use(bodyparse.json());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Request-Method', '*');
  res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE, PUT');
  res.header('Access-Control-Allow-Headers', '*');
  next();
});
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: '192.168.99.100',
  user: 'docker_practice',
  password: 'asdfghjk',
  database: 'Wow_Library'
});

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});


app.post('/register', function (req, res) {
  
  var newarray = [req.body.Role, req.body.Email, req.body.Username, req.body.Password, req.body.Name, req.body.Gender, req.body.Age, req.body.Address, req.body.State, req.body.City, req.body.Idtype];
  console.log(newarray);
  connection.query("insert into users (Role,Email,Username,Password,Name,Gender,Age,Address,State,City,Idtype) values (?)", [newarray], function (error, results) {
  
    if (error) {
      res.status(501).send(error);
      console.log("error");
    }
    else {
      let resultText= [{ 'Role': req.body.Role, 'Username': req.body.Name, 'password': req.body.Password }]
      res.send(resultText);
      console.log("success");
    }
  });
})

app.post('/login', function (req, res) {
  var username=[req.body.Username];
  var password=[req.body.Password];
  connection.query("select * from users where Username = ? and Password = ?",[username,password], function (error, results1) {
    if (error) {
      res.status(501).send(error);
    }
    else {
      if(results1.length === 0){
         res.status(401);
      }
      res.send(results1);
      console.log("resultText"+results1);
    }
  });
})

app.put('/:Username', function (req, res) {
  console.log(req.body);
  console.log(req.params.Username);
  // updating_values=[req.body.Email ,req.body.Username ,req.body.Password ,req.body.Name ,req.body.Age ,req.body.Age ,req.body.Address ,req.body.State ,req.body.City]

  connection.query("update users set Email = ?,Username = ?,Password = ?,Name = ?,Age = ?,Address = ?,State = ?,City = ? where Username = ?", [req.body.Email,req.body.Username,req.body.Password ,req.body.Name ,req.body.Age ,req.body.Address ,req.body.State ,req.body.City,req.params.Username], function (error, results) {
      if (error) { res.status(400).send(error); }
      else {
          console.log('The result is: ', results);
          let result = [{ 'Email':req.body.Email, 
                          'Username': req.body.Username,
                          'Password': req.body.Password,
                          'Name':req.body.Name,
                          'Age':req.body.Age,
                          'State':req.body.State,
                          'City':req.body.City,
                        }]
          console.log("put method :"+result);
          res.send(result); 
      }
  });
})

app.get('/:Username', function (req, res) {
  var arr=[req.params.Username]
  connection.query("select * from  users where Username = ?",arr, function (error1, results1) {
    if (error1) { res.status(501).send(error1); }
    else {
      res.send(results1);
    }
  });
})
app.listen(3014);
